package services.channel

import java.beans.PropertyChangeEvent

import akka.actor.{Actor, ActorLogging, Props}
import org.asteriskjava.live.AsteriskChannel
import org.asteriskjava.manager.action.{GetVarAction, UnpauseMonitorAction, PauseMonitorAction}
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.{GetVarResponse, ManagerResponse}
import play.api.Logger
import services.XucAmiBus
import services.XucAmiBus._
import org.asteriskjava.live.{ChannelState => AsteriskChannelState}
import services.channel.ChannelManager.Channels
import xivo.xucami.models._

import scala.collection.immutable.HashMap


object ChannelRepo {
  val logger = Logger(ChannelRepo.getClass.getName)

  def addNewChannel(channel: Channel, channelRepo: Channels): Channels = channelRepo.get(channel.id) match {
    case None =>
      logger.debug(s"Adding New Channel : $channel")
      channelRepo + (channel.id -> channel)
    case _ =>
      logger.debug(s"Channel : $channel already exists")
      channelRepo
  }
}

object ChannelManager {
  type Channels = HashMap[String, Channel]
  val defaultAmiBus = XucAmiBus.amiEventBus
  val props = Props(new ChannelManager(defaultAmiBus,ChannelRepo.addNewChannel) with ProductionEventProcessor)
}

trait ProductionEventProcessor extends EventProcessor {
  override val processor = new AmiEventProcessor(XucAmiBus.amiEventBus)
}

class ChannelManager(amiBus: XucAmiBus, addNewChannel:(Channel,Channels)=>Channels = ChannelRepo.addNewChannel,
                     channels: Channels = new HashMap[String, Channel]()) extends Actor with ActorLogging {
  this: EventProcessor =>

  amiBus.subscribe(self, AmiType.AmiEvent)
  amiBus.subscribe(self, AmiType.AmiResponse)
  amiBus.subscribe(self, AmiType.ChannelActionRequest)
  import ChannelManager.Channels

  val loggerAmi = Logger("amievents")

  def receive = process(channels)

  def process(channels: Channels): Receive = {

    case AmiEvent(newChanEvent: NewChannelEvent) =>
      AmiLogger.logEvent(AmiEvent(newChanEvent))
      context.become(process(addNewChannel(onNewChannel(newChanEvent), channels)))

    case event: AmiEvent =>
      AmiLogger.logEvent(event)
      context.become(process(processor.process(event, ChannelRepository(channels))))

    case request: ChannelRequest =>
      processChannelRequest(request, channels)

    case reply: AmiResponse =>
      context.become(process(processResponse(reply, channels)))

    case propChange: PropertyChangeEvent =>
      context.become(process(processPropertyChangeEvent(propChange, channels)))

    case any =>
      log.debug(s"Received an unprocessed message: $any")
  }

  private def onNewChannel(newChanEvent: NewChannelEvent) = {
    val channel = Channel(newChanEvent)
    publish(channel)
    channel
  }

  private def publish(channel:Channel) = {
    loggerAmi.info(s"Pub: $channel")
    amiBus.publish(ChannelEvent(channel))
  }

  private def processMonitorUpdate(channel: AsteriskChannel, newValue: Boolean, channels: Channels): Channels = {
    val uniqueId = channel.getId
    channels.get(uniqueId) match {
      case Some(channel) =>
        if (newValue) {
          channel.monitored = MonitorState.ACTIVE
          log.debug(s"Channel $channel is monitored")
        }
        else {
          channel.monitored = MonitorState.DISABLED
          log.debug(s"Channel $channel is not monitored")
        }
        publish(channel)
        channels - uniqueId + (uniqueId -> channel)
      case None =>
        log.debug(s"Got a MonitorUpdate for an unknown channel: $channel.")
        channels
    }
  }


  private def processChannelRequest(request: ChannelRequest, channels: Channels) {
    request.message.action match {
      case ChannelAction.PauseMonitor =>
        val action = new PauseMonitorAction()
        val channel = findChannelByAgentNumber(channels, request.message.agentNumber)
        channel match {
          case Some(channel) =>
            action.setChannel(channel.name)
            amiBus.publish(AmiAction(action, Some(channel.id)))
          case None =>
            log.warning(s"Received action $action for unknown agent")
        }

      case ChannelAction.UnpauseMonitor =>
        val action = new UnpauseMonitorAction()
        val channel = findChannelByAgentNumber(channels, request.message.agentNumber)
        channel match {
          case Some(channel) =>
            action.setChannel(channel.name)
            amiBus.publish(AmiAction(action, Some(channel.id)))
          case None =>
            log.warning(s"Received action $action for unknown agent")
        }

      case any =>
        log.info(s"Unprocessed channel action request: $any")
    }
  }

  private def findChannelByAgentNumber(channels: Channels, agentNumber: Option[String]): Option[Channel] =
      channels.values find( _.agentNumber.equals(agentNumber))


  case class AmiR(response:ManagerResponse, action:Option[AmiAction], channels: Channels)

  type AmiProcessRespFunction = scala.PartialFunction[AmiR, Channels]

  def processPauseMonitorResp : AmiProcessRespFunction = {
    case AmiR(response: ManagerResponse, Some(action), channels) =>
    action.message match {
      case pause: PauseMonitorAction =>
        channels.get(action.reference.getOrElse("")) match {
          case Some(channel) =>
            channel.monitored = MonitorState.PAUSED
            publish(channel)
            channels - channel.id + (channel.id -> channel)
          case None =>
            log.error(s"Got response for unknown channel!")
            channels
        }

      case unpause: UnpauseMonitorAction =>
        channels.get(action.reference.getOrElse("")) match {
          case Some(channel) =>
            channel.monitored = MonitorState.ACTIVE
            publish(channel)
            channels - channel.id + (channel.id -> channel)
          case None =>
            log.error(s"Got response for unknown channel!")
            channels
        }
      case any =>
        log.debug(s"Reply $any to action $action is not currently processed ${channels.get(action.reference.getOrElse(""))}")
        channels

    }
  }

  def processGetVarResp : AmiProcessRespFunction = {
    case AmiR(response :GetVarResponse, Some(action), channels) =>
      log.debug(s"${response}   \n\t${response.getAttributes}  \n\t$channels")
      channels.get(action.reference.getOrElse("")) match {
        case Some(channel) =>
          log.debug(s""" ${response.getAttribute("variable")} : ${response.getAttribute("value")}""")
          if (response.getAttribute("variable") == "MONITOR_PAUSED" && response.getAttribute("value") == "true") {
            log.debug("Monitor started in pause state")
            channel.monitored = MonitorState.PAUSED
            publish(channel)
            channels - channel.id + (channel.id -> channel)
          }
          channels
        case None =>
          channels
      }
  }

  def processAny : AmiProcessRespFunction = {
    case AmiR(response, Some(action), channels) => channels
  }

  private def processResponse(reply: AmiResponse, channels: Channels): Channels = {
    val attr = reply.message._1.getAttributes
    log.debug(s"ami response: $reply : $attr")
    reply.message match {
      case (response, Some(action)) =>
        response.getResponse match {
          case "Success" =>
            (processGetVarResp orElse processPauseMonitorResp orElse processAny) (AmiR(response,Some(action),channels))
          case any =>
            log.error(s"Ami action $action failed: $response")
            channels
        }
      case (response, None) =>
        log.debug(s"Not processing replies without action: $response")
        channels
    }
  }

  private def processPropertyChangeEvent(propChange: PropertyChangeEvent, channels: Channels): Channels = {
    propChange.getSource match {
      case channel: AsteriskChannel=>
        propChange.getPropertyName match {
          case AsteriskChannel.PROPERTY_MONITORED =>
            amiBus.publish(AmiAction(new GetVarAction(channel.getName,"MONITOR_PAUSED"),Some(channel.getId)))
            processMonitorUpdate(channel, propChange.getNewValue.asInstanceOf[Boolean], channels)
          case _ =>
            log.debug(s"Received unprocessed property change event: $propChange")
            channels
        }
      case _ =>
        log.error(s"Received wrong source in propertyChangeEvent for monitor property: $propChange")
        channels
    }
  }

}
