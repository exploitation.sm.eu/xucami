package services

import akka.event.ActorEventBus
import akka.event.SubchannelClassification
import akka.util.Subclassification
import org.asteriskjava.manager.action.OriginateAction
import play.api.Logger
import xivo.xucami.models.{ChannelActionRequest, Channel}
import xivo.xucami.userevents.UserEventAgent

object XucAmiBus {
  type AmiObjectReference = String
  type XucManagerEvent = org.asteriskjava.manager.event.ManagerEvent
  type XucManagerAction = (org.asteriskjava.manager.action.ManagerAction)
  type XucManagerResponse = (
    org.asteriskjava.manager.response.ManagerResponse, Option[AmiAction]
  )

  val OriginateTimeout = 8000L

  object AmiType {
    val separator = "/"
    val AmiEvent = separator + "AmiEvent"
    val AmiAction = separator + "AmiAction"
    val AmiResponse = separator + "AmiResponse"
    val ChannelEvent = separator + "ChannelEvent"
    val ChannelActionRequest = separator + "ChannelActionRequest"
    val SetVarActionRequest = separator + "SetVarActionRequest"
    val ListenActionRequest = separator + "ListenActionRequest"
    val UserEventAgent = separator + "UserEventAgent"
    val OutBoundDial = separator + "OutBoundDial"
    val AmiRequest = separator + "AmiRequest"
    val QueueEvent = separator + "QueueEvent"
  }

  abstract trait AmiMessage {
    val message: Any
    val classifier: String
  }
  case class AmiEvent(message: XucManagerEvent) extends AmiMessage { val classifier = AmiType.AmiEvent }
  case class AmiUserEventAgent(message: UserEventAgent) extends AmiMessage { val classifier = AmiType.UserEventAgent }
  case class AmiAction(message: XucManagerAction, reference: Option[AmiObjectReference] = None)
    extends AmiMessage { val classifier = AmiType.AmiAction }
  case class AmiResponse(message: XucManagerResponse) extends AmiMessage { val classifier = AmiType.AmiResponse}
  case class ChannelEvent(message: Channel) extends AmiMessage { val classifier = AmiType.ChannelEvent}

  case class SpyChannels(spyerChannel: Channel, spyeeChannel: Channel)
  case class SpyStarted(message:SpyChannels) extends AmiMessage { val classifier = AmiType.ChannelEvent}
  case class SpyStopped(message: Channel) extends AmiMessage { val classifier = AmiType.ChannelEvent}

  case class ChannelRequest(message: ChannelActionRequest)
    extends AmiMessage { val classifier = AmiType.ChannelActionRequest}

  abstract trait AmiActionRequest {
    def buildAction():XucManagerAction
  }
  case class SetVarRequest(message:SetVarActionRequest)
    extends AmiMessage { val classifier = AmiType.SetVarActionRequest}
  case class SetVarActionRequest(name: String, value: String)

  case class ListenRequest(message: ListenActionRequest)
    extends AmiMessage { val classifier = AmiType.ListenActionRequest}

  case class ListenActionRequest(listener:String, listened:String) {
    def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(listener)
      action.setAsync(true)
      action.setData(s"$listened,bds")
      action.setCallerId("Listen")
      action.setApplication("ChanSpy")
      action
    }
  }
  case class AmiRequest(message: AmiActionRequest)
    extends AmiMessage { val classifier = AmiType.AmiRequest}

  case class OutBoundDialActionRequest(destination:String, skill:String, queueNumber: String, variables: Map[String, String], xivoHost: String)
    extends AmiActionRequest {
    def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(s"Local/$queueNumber@default/n")
      action.setAsync(true)
      action.setContext("default")
      action.setExten(destination)
      action.setPriority(1)
      action.setCallerId(destination)
      action.setTimeout(OriginateTimeout)
      action.setVariable("XIVO_QUEUESKILLRULESET",skill)
      action.setVariable("__SIPADDHEADER51",s"Call-Info:<sip:$xivoHost>;answer-after=0")
      action.setVariable("XUC_CALLTYPE","OutboundOriginate")
      setUserVariables(action)
      action
    }

    private def setUserVariables(action: OriginateAction): Unit = for((k, v) <- variables) action.setVariable(s"USR_$k", v)
  }

  case class DialActionRequest(sourceChannel: String, sourceContext: String, destination:String, variables: Map[String, String], xivoHost: String)
    extends AmiActionRequest {
    def buildAction(): OriginateAction = {
      val action = new OriginateAction()
      action.setChannel(sourceChannel)
      action.setAsync(true)
      action.setPriority(1)
      action.setTimeout(OriginateTimeout)
      action.setContext(sourceContext)
      action.setCallerId(destination)
      action.setExten(destination)
      action.setVariable("__SIPADDHEADER51",s"Call-Info:<sip:$xivoHost>;answer-after=0")
      action.setVariable("XUC_CALLTYPE","Originate")
      setUserVariables(action)
      action
    }

    private def setUserVariables(action: OriginateAction): Unit = for((k, v) <- variables) action.setVariable(s"USR_$k", v)
  }


  val amiEventBus = new XucAmiBus
}

class XucAmiBus extends ActorEventBus with SubchannelClassification {
  type Event = XucAmiBus.AmiMessage
  type Classifier = String

  protected def subclassification = new Subclassification[Classifier] {
    def isEqual(x: Classifier, y: Classifier) = x.equals(y)

    def isSubclass(x: Classifier, y: Classifier) = {
      if (y.length == 0) {
        false
      }
      else {
        x.startsWith(y)
      }
    }
  }

  override def classify(event: Event) = event.classifier

  protected def publish(event: Event, subscriber: Subscriber) = subscriber ! event

  override def subscribe(subscriber: Subscriber, to: Classifier) = {
    Logger.debug(s"subscribe : $subscriber to : $to")
    super.subscribe(subscriber, to)
  }
}

