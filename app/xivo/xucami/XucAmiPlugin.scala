package xivo.xucami

import com.codahale.metrics.{Slf4jReporter, SharedMetricRegistries}
import play.api._
import java.util.concurrent.TimeUnit
import play.api.libs.concurrent.Akka
import play.api.Play.current

class XucAmiPlugin(val app: Application) extends Plugin {

  override def onStart() {
    Logger.info("Starting xuc ami")
    Logger.info("-------------parameters--------------------")
    Logger.info(s"IpAddress               : ${XucAmiConfig.ipAddress}")
    Logger.info(s"Port                    : ${XucAmiConfig.port}")
    Logger.info(s"Username                : ${XucAmiConfig.username}")
    Logger.info(s"Secret                  : ${XucAmiConfig.secret}")
    Logger.info(s"statsLogReporter        : ${XucAmiConfig.metricsLogReporter}")
    Logger.info(s"statsLogReporterPeriod  : ${XucAmiConfig.metricsLogReporterPeriod}")

    Akka.system.actorOf(AmiSupervisor.props, "XucAmiManagerConnector")

  }

  override def onStop() {
    Logger.info("Xuc ami Application shutdown...")
  }

  private def startTechMetrics {
    val registry = SharedMetricRegistries.getOrCreate(XucAmiConfig.metricsRegistryName)

    if (XucAmiConfig.metricsLogReporter) {
      val reporter= Slf4jReporter.forRegistry(registry)
        .outputTo(Logger.logger)
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .build()
      reporter.start(XucAmiConfig.metricsLogReporterPeriod, TimeUnit.MINUTES)
    }
  }

  override def enabled = {
    XucAmiConfig.enabled match {
      case false =>
        Logger.info("xuc ami disabled by the configuration, not starting")
        false
      case _ => true

    }
  }
}
