package xivo.xucami.ami

import java.beans.{PropertyChangeEvent, PropertyChangeListener}

import akka.actor.ActorRef
import org.asteriskjava.live._
import org.asteriskjava.live.internal.AsteriskAgentImpl
import play.api.Logger.logger
import xivo.xucami.models.Channel

class AsteriskLiveListener(liveServerConnector: ActorRef) extends AsteriskServerListener with PropertyChangeListener {

  def onNewAsteriskChannel(astChannel: AsteriskChannel)  {
    val channel = Channel(astChannel)
    logger.debug(s"New asterisk channel: $astChannel")
    astChannel.addPropertyChangeListener(this)
    liveServerConnector ! channel
  }

  def onNewMeetMeUser(user: MeetMeUser)  {
    logger.debug(s"New meet me user: $user, not processing")
  }

  def onNewQueueEntry(queueEntry: AsteriskQueueEntry) {
    logger.debug(s"New queue entry: $queueEntry, not processing")
  }

  def onNewAgent(agent: AsteriskAgentImpl) {
    logger.debug(s"New agent: $agent, not processing")
  }

  def propertyChange(propertyChangeEvent: PropertyChangeEvent) {
    logger.debug(s"New propertyChangeEvent: $propertyChangeEvent")
    liveServerConnector ! propertyChangeEvent
  }

}
