import sbt._

object Version {
  val asteriskjava = "2.0.0.a.XIVOCC"
  val metrics = "3.1.0"
  val metricsjvm = metrics
  val scalaTest = "2.2.4"
  val mockito = "1.10.19"
  val akka = "2.3.4"
}

object Library {

  val asteriskjava      = ("org.asteriskjava"               %  "asterisk-java"          % Version.asteriskjava
    exclude("javax.jms", "jms") exclude("com.sun.jdmk", "jmxtools") exclude("com.sun.jmx", "jmxri"))
  val metrics           = "io.dropwizard.metrics"          % "metrics-core"            % Version.metrics
  val metricsjvm        = "io.dropwizard.metrics"          % "metrics-jvm"             % Version.metricsjvm
  val akkaTestkit       = "com.typesafe.akka"              %% "akka-testkit"           % Version.akka
  val scalaTest         = "org.scalatest"                  %% "scalatest"              % Version.scalaTest
  val mockito           = "org.mockito"                    %  "mockito-all"            % Version.mockito
}

object Dependencies {
  import Library._

  val scalaVersion = "2.11.6"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us")
  )

  val runDep = run(
    asteriskjava,
    metrics,
    metricsjvm
  )

  val testDep = test(
    scalaTest,
    akkaTestkit,
    mockito
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
}
