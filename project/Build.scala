import sbt._
import Keys._

object ApplicationBuild extends Build {

  val appName = "xucami"
  val appVersion = "1.11.1"
  val appOrganisation = "xivo"

  val main = Project(appName, file("."))
    .enablePlugins(play.PlayScala)
    .settings(
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      resolvers     ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      sbt.Keys.fork in Test := false
    )
}
