package services

import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.asteriskjava.manager.event.{AgentCompleteEvent, AgentConnectEvent}
import org.asteriskjava.manager.response.CoreStatusResponse
import org.scalatest.BeforeAndAfter
import org.scalatest.mock.MockitoSugar
import services.XucAmiBus._
import xivo.xucami.models.{Channel, CallerId}

class XucAmiBusSpec extends TestKitSpec("XucAmiBusSpec")
with BeforeAndAfter with MockitoSugar {

  var statsBus: XucAmiBus = _
  var actor: TestProbe = null

  before {
    statsBus = new XucAmiBus
    actor = TestProbe()
  }

  "XucAmiBus" should {

    "send event to the subscriber on channel" in {

      val mockEvent = AmiEvent(new AgentConnectEvent("test"))
      statsBus.subscribe(actor.ref, AmiType.AmiEvent)
      statsBus.publish(mockEvent)

      actor.expectMsg(mockEvent)
    }

    "not send event to the subscriber on other channel" in {
      val mockEvent = AmiEvent(new AgentCompleteEvent("test"))
      statsBus.subscribe(actor.ref, AmiType.AmiResponse)
      statsBus.publish(mockEvent)

      actor.expectNoMsg
    }

    "send events to the subscriber on all channels" in {
      val amiEvent = AmiEvent(new AgentCompleteEvent("test"))
      val amiResponse = AmiResponse(new CoreStatusResponse(), None)
      statsBus.subscribe(actor.ref, AmiType.separator)
      statsBus.publish(amiEvent)
      statsBus.publish(amiResponse)

      actor.expectMsgAllOf(amiEvent, amiResponse)
    }

    "not send events to the subscriber on empty string" in {
      val amiEvent = AmiEvent(new AgentCompleteEvent("test"))
      val amiResponse = AmiResponse(new CoreStatusResponse(), None)
      statsBus.subscribe(actor.ref, "")
      statsBus.publish(amiEvent)
      statsBus.publish(amiResponse)

      actor.expectNoMsg
    }

    "implement equality of case classes like ChannelEvent" in {
      val event1 = ChannelEvent(new Channel("1", "a", CallerId("b", "2"),"1"))
      val event2 = ChannelEvent(new Channel("1", "a", CallerId("b", "2"),"1"))
      event1.equals(event2) shouldBe true
    }

  }

  "OutboundDialAction" should {
    "build originate" in {
      val queueNumber = "3000"
      val destination = "1200"
      val skill= "agent_sortant(agent=agent_67)"
      val variables = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost = "192.168.12.145"
      val action = OutBoundDialActionRequest(destination, skill, queueNumber, variables, xivoHost)

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/$queueNumber@default/n"
      originate.getContext shouldBe "default"
      originate.getExten shouldBe destination
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe destination
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get("XIVO_QUEUESKILLRULESET") shouldBe skill
      originate.getVariables.get("__SIPADDHEADER51") shouldBe s"Call-Info:<sip:$xivoHost>;answer-after=0"
      originate.getVariables.get("XUC_CALLTYPE") shouldBe "OutboundOriginate"
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
    }
  }

  "DialAction" should {
    "build originate" in {
      val sourceChannel = "SIP/456re"
      val destination = "1200"
      val context = "default"
      val variables = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost = "192.168.12.145"
      val action = DialActionRequest(sourceChannel, context, destination, variables, xivoHost)

      val originate = action.buildAction()

      originate.getChannel shouldBe "SIP/456re"
      originate.getContext shouldBe "default"
      originate.getExten shouldBe "1200"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe destination
      originate.getVariables.get("__SIPADDHEADER51") shouldBe s"Call-Info:<sip:$xivoHost>;answer-after=0"
      originate.getVariables.get("XUC_CALLTYPE") shouldBe "Originate"
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
    }
  }
}
