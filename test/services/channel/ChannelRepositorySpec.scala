package services.channel

import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import xivo.xucami.models.{ChannelState, CallerId, Channel}
import xuctest.BaseTest

import scala.collection.immutable.HashMap


class ChannelRepositorySpec extends BaseTest with MockitoSugar {

  "A channel repo" should {

    "Update a channel" in {
      val initialChannel = Channel("13216545.12", "SIP/wred", CallerId("tony", "2016"), agentNumber=Some("2500"), linkedChannelId = "13216545.12")

      ChannelRepository(HashMap(initialChannel.id -> initialChannel))
        .updateChannel(initialChannel.withChannelState(ChannelState.BUSY))
        .channels(initialChannel.id) should be(initialChannel.withChannelState(ChannelState.BUSY))
    }
    "Update a channel and apply function" in {
      val doWithChannel = mock[Channel=>Channel]
      val initialChannel = Channel("13216545.12", "SIP/wred", CallerId("tony", "2016"), agentNumber=Some("2500"), linkedChannelId = "13216545.12")

      ChannelRepository(HashMap(initialChannel.id -> initialChannel))
        .updateChannel(initialChannel.withChannelState(ChannelState.BUSY),doWithChannel)

      verify(doWithChannel)(initialChannel.withChannelState(ChannelState.BUSY))

    }
    "Update linkedid of a channel" in {

      val initialChannel = Channel("13216545.12", "SIP/wred", CallerId("tony", "2016"), agentNumber=Some("2500"), linkedChannelId = "13216545.12")
      val destChannel = Channel("14665454.54", "SIP/1k4yj2-00000038", CallerId("", ""), linkedChannelId = "14665454.54")

      val chans = HashMap(initialChannel.id -> initialChannel, destChannel.id -> destChannel)

      ChannelRepository(chans).updateLinkedId(initialChannel.id,destChannel.id).channels(destChannel.id).linkedChannelId should be(initialChannel.linkedChannelId)

    }
    "update linkedid of a channel and its linked channels" in {
      val initialChannel = Channel("13216545.12", "SIP/wred", CallerId("tony", "2016"), agentNumber=Some("2500"), linkedChannelId = "13216545.04")
      val destChannel = Channel("14665454.54", "SIP/1k4yj2-00000038", CallerId("", ""), linkedChannelId = "14665454.54")
      val destChannellinked = Channel("14665454.32", "SIP/1k4yj2-00000044", CallerId("", ""), linkedChannelId = "14665454.54")
      val chans = HashMap(initialChannel.id -> initialChannel, destChannel.id -> destChannel, destChannellinked.id -> destChannellinked)

      ChannelRepository(chans).updateLinkedId(initialChannel.id,destChannel.id).channels(destChannellinked.id).linkedChannelId should be(initialChannel.linkedChannelId)

    }

    "propagate variables" in {
      val channelId1 = "98798799.10"
      val channelId2 = "98798799.11"
      val channel1 = Channel(channelId1, "Local/id-58@agentcallback-0000023e;1", CallerId("bob", "1204"),
        variables = Map("varch1" -> "valuech1"),
        linkedChannelId = channelId1
      )
      val channel2 = Channel(channelId2, "Local/id-58@agentcallback-0000023e;2", CallerId("bob", "1204"),channelId2,
        variables = Map("varch2" -> "valuech2")
      )
      val chans = HashMap(channel1.id -> channel1, channel2.id -> channel2)

      val updatedChan = ChannelRepository(chans).propagateVars(channelId1,channelId2).channels(channelId2)

      updatedChan.variables("varch1") should be("valuech1")
      updatedChan.variables("varch2") should be("valuech2")

    }
  }

}
