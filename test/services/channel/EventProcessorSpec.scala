package services.channel

import org.asteriskjava.manager.event._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import services.XucAmiBus
import services.XucAmiBus._
import services.channel.ChannelManager._
import xivo.xucami.models.{MonitorState, ChannelState, CallerId, Channel}
import xuctest.BaseTest

import scala.collection.immutable.HashMap

class EventProcessorSpec extends BaseTest with MockitoSugar {

  class Helper() {
    val agentCalled = new AgentCalledEvent("")
    agentCalled.setAgentCalled("Local/id-6@agentcallback")
    agentCalled.setAgentName("Agent/2200")
    agentCalled.setUniqueId("1416910889.110")
    agentCalled.setQueue("irs")
    agentCalled.setCallerIdName("hawkeye")
    agentCalled.setCallerIdNum("1002")

    val amiBus = mock[XucAmiBus]

    val eventProcessor = new AmiEventProcessor(amiBus)
  }

  "Ami event processor on agent called" should {
    "create a channel if not exists" in new Helper() {

      agentCalled.setDestinationChannel("Local/id-6@agentcallback-0000001e;1")

      var chans: Channels = HashMap()

      val expectedChannel = Channel("1416910889.110", "Local/id-6@agentcallback-0000001e;1", CallerId("hawkeye", "1002"),"1416910889.110", agentNumber = Some("2200"))

      var updatedChannels = eventProcessor.process(AmiEvent(agentCalled), ChannelRepository(chans))

      updatedChannels(expectedChannel.id) should be(expectedChannel)
      verify(amiBus).publish(ChannelEvent(expectedChannel))
    }
  }

  "update an existing channel with agent/queue info" in new Helper() {

    val channel = Channel("1416910889.110","Local/id-6@agentcallback-0000001e;1",CallerId("hawkeye", "1002"),"1416910889.110")

    var chans: Channels = HashMap(channel.id -> channel)

    var updatedChannels = eventProcessor.process(AmiEvent(agentCalled), ChannelRepository(chans))

    updatedChannels(channel.id).agentNumber should be(Some("2200"))

    verify(amiBus).publish(ChannelEvent(updatedChannels(channel.id)))
  }

  "update destination Channel with calling channel variables on agentcalled" in new Helper {

    val destinationChannel = Channel("1416910889.108", "Local/3099@default-0000001d;1", CallerId("qldfs", "1002"),"1416910889.108")
    val agentChannel = Channel("1416910889.110", "Local/id-6@agentcallback-0000001e;1", CallerId("hawkeye", "1002"),"1416910889.108",
      variables = Map("callingvar" -> "callingvalue")
    )
    agentCalled.setDestinationChannel(destinationChannel.name)

    val chans: Channels = HashMap(agentChannel.id -> agentChannel, destinationChannel.id -> destinationChannel)

    var updatedChannels = eventProcessor.process(AmiEvent(agentCalled), ChannelRepository(chans))

    updatedChannels(destinationChannel.id).variables should be(agentChannel.variables)
  }

  """update destination Channel with
        - calling channel linked channel
        - update destination bridged channel with
        - calling channel linked channel""" in new Helper {

    val destinationBridgedChannel = Channel("1416910889.104", "Local/3099@default-0000001d;2", CallerId("qldfs", "1002"),
      linkedChannelId = "1416910889.108"
    )
    val destinationChannel = Channel("1416910889.108", "Local/3099@default-0000001d;1", CallerId("qldfs", "1002"),
      linkedChannelId = "1416910889.108"
    )
    val agentChannel = Channel("1416910889.110", "Local/id-6@agentcallback-0000001e;1", CallerId("hawkeye", "1002"),
      linkedChannelId = "99897798978.33"
    )
    agentCalled.setUniqueId(agentChannel.id)
    agentCalled.setDestinationChannel(destinationChannel.name)

    val chans: Channels = HashMap(agentChannel.id -> agentChannel, destinationChannel.id -> destinationChannel, destinationBridgedChannel.id -> destinationBridgedChannel)

    var updatedChannels = eventProcessor.process(AmiEvent(agentCalled), ChannelRepository(chans))

    updatedChannels(destinationChannel.id).linkedChannelId should be(agentChannel.linkedChannelId)

    updatedChannels(destinationBridgedChannel.id).linkedChannelId should be(agentChannel.linkedChannelId)
  }
  "Ami event processor on new state" should {
    "update channel state and variables from linkedid and publish" in new Helper {
      val channelId = "5646546546.12"
      val linkedId = "5646546546.10"

      val newState = new NewStateEvent("")
      newState.setUniqueId(channelId)
      newState.setCallerIdName("hawkeye")
      newState.setCallerIdNum("1002")
      newState.setChannelState(3)
      newState.setConnectedLineNum("44200")

      val linkedChannel = Channel(linkedId, "Local/id-28@agentcallback-0000003e;2", CallerId("", ""), linkedChannelId = linkedId,
        variables = HashMap("Var" -> "Value")
      )
      val channel = Channel(channelId, "Local/id-28@agentcallback-0000003e;1", CallerId("hawkeye", "1002"),linkedChannelId = linkedId )

      val chans = eventProcessor.process(AmiEvent(newState), ChannelRepository(HashMap(channelId -> channel, linkedChannel.id -> linkedChannel)))

      chans(channelId).state should be(ChannelState(3))
      chans(channelId).connectedLineNb shouldBe(Some("44200"))
      chans(channelId).variables should be(HashMap("Var" -> "Value"))
      verify(amiBus).publish(ChannelEvent(chans(channelId)))
    }
    "republish linked channel on state UP" in new Helper {
      val channelId = "5646546546.12"
      val linkedId = "5646546546.10"

      val newState = new NewStateEvent("")
      newState.setUniqueId(channelId)
      newState.setCallerIdName("hawkeye")
      newState.setCallerIdNum("1002")
      newState.setChannelState(6)
      newState.setConnectedLineNum("44200")

      val linkedChannel = Channel(linkedId, "Local/id-28@agentcallback-0000003e;2", CallerId("", ""), linkedChannelId = linkedId,
        variables = HashMap("Var" -> "Value")
      )
      val channel = Channel(channelId, "Local/id-28@agentcallback-0000003e;1", CallerId("hawkeye", "1002"),
        linkedChannelId = linkedId
      )

      val chans = eventProcessor.process(AmiEvent(newState), ChannelRepository(HashMap(channelId -> channel, linkedChannel.id -> linkedChannel)))

      verify(amiBus).publish(ChannelEvent(chans(channelId)))
      verify(amiBus).publish(ChannelEvent(chans(linkedId).addVariables(Channel.OtherEndUp)))

    }
    "Do not publish if linked channel not exists (start in traffic)" in new Helper {
      val channelId = "5646546546.12"
      val linkedId = "5646546546.10"

      val newState = new NewStateEvent("")
      newState.setUniqueId(channelId)
      newState.setCallerIdName("hawkeye")
      newState.setCallerIdNum("1002")
      newState.setChannelState(6)
      newState.setConnectedLineNum("44200")

      val channel = Channel(channelId, "Local/id-28@agentcallback-0000003e;1", CallerId("hawkeye", "1002"),
        linkedChannelId = linkedId
      )

      val chans = eventProcessor.process(AmiEvent(newState), ChannelRepository(HashMap(channelId -> channel)))

      verify(amiBus).publish(ChannelEvent(chans(channelId)))
      verifyNoMoreInteractions(amiBus)
    }
  }

  "Ami event processor on channel renamed state" should {
    "rename channel" in new Helper {
      val channelId = "98798799.1"

      val renamed = new RenameEvent("")
      renamed.setUniqueId(channelId)
      renamed.setOldname("Local/id-58@agentcallback-0000023e;1")
      renamed.setNewname("SIP/ihvbur-00000009")

      val channel = Channel(channelId,"Local/id-58@agentcallback-0000023e;1",CallerId("bob", "1204"),channelId)

      val chans = eventProcessor.process(AmiEvent(renamed), ChannelRepository(HashMap(channelId -> channel)))

      chans(channelId).name should be("SIP/ihvbur-00000009")

    }
  }

  "Ami event processor on set var event" should {
    "update channel variable on var set event" in new Helper {
      val channelId = "98798799.1"

      val channel = Channel(channelId, "Local/id-58@agentcallback-0000023e;1", CallerId("bob", "1204"),channelId)

      val varSetEvent = new VarSetEvent("")
      varSetEvent.setUniqueId(channelId)
      varSetEvent.setVariable("VARNAME")
      varSetEvent.setValue("VALUE")

      val chans = eventProcessor.process(AmiEvent(varSetEvent), ChannelRepository(HashMap(channelId -> channel)))

      chans(channelId).variables("VARNAME") should be(("VALUE"))

    }
    "do not update variable on null value" in new Helper {
      val channelId = "98798799.1"

      val channel = Channel(channelId, "Local/id-58@agentcallback-0000023e;1", CallerId("bob", "1204"),channelId)

      val varSetEvent = new VarSetEvent("")
      varSetEvent.setUniqueId(channelId)
      varSetEvent.setVariable("VARNAMENULL")
      varSetEvent.setValue(null)

      val chans = eventProcessor.process(AmiEvent(varSetEvent), ChannelRepository(HashMap(channelId -> channel)))

      chans(channelId).variables.get("VARNAMENULL") should be(None)
    }

  }

  "Ami event processor on local bridge event" should {
    "propagate channel1 variables to channel2 on localbridge event and set linkedid" in new Helper {
      val channelId1 = "98798799.10"
      val channelId2 = "98798799.11"
      val channel1 = Channel(channelId1, "Local/id-58@agentcallback-0000023e;1", CallerId("bob", "1204"),
        variables = Map("varch1" -> "valuech1"),
        linkedChannelId = channelId1
      )
      val channel2 = Channel(channelId2, "Local/id-58@agentcallback-0000023e;2", CallerId("bob", "1204"), channelId2,
        variables = Map("varch2" -> "valuech2")
      )

      var localBridgeEvent = new LocalBridgeEvent("")
      localBridgeEvent.setUniqueId1(channelId1)
      localBridgeEvent.setUniqueId2(channelId2)

      val chans = eventProcessor.process(AmiEvent(localBridgeEvent), ChannelRepository(HashMap(channelId1 -> channel1, channelId2 -> channel2)))

      chans(channelId1).variables("varch1") should be("valuech1")
      chans(channelId2).variables("varch1") should be("valuech1")
      chans(channelId2).variables("varch2") should be("valuech2")
      chans(channelId2).linkedChannelId should be(channelId1)

    }

  }

  trait SpyFixture {
    val spyeeChannel = Channel("413313189.1", "SIP/çhdfg-00000002f", CallerId("Jack", "3000"),"413313189.1")
    val spyerChannel = Channel("123564464.1", "SIP/qgsdh-000000145", CallerId("Alice", "2000"),"123564464.1")

  }

  "Ami event processor spy events" should {
    "publish a start spy event on spy channel started " in new Helper with SpyFixture {

      val spyStart = new ChanSpyStartEvent("")
      spyStart.setSpyeeChannel(spyeeChannel.name)
      spyStart.setSpyerChannel(spyerChannel.name)

      eventProcessor.process(AmiEvent(spyStart), ChannelRepository(HashMap(spyeeChannel.id -> spyeeChannel, spyerChannel.id -> spyerChannel)))

      verify(amiBus).publish(SpyStarted(SpyChannels(spyerChannel, spyeeChannel)))

    }

    "publish a stop spy event on spy channel stopped" in new Helper with SpyFixture {

      val spyStopped = new ChanSpyStopEvent("")
      spyStopped.setSpyeeChannel(spyeeChannel.name)

      eventProcessor.process(AmiEvent(spyStopped), ChannelRepository(HashMap(spyeeChannel.id -> spyeeChannel, spyerChannel.id -> spyerChannel)))

      verify(amiBus).publish(SpyStopped(spyeeChannel))

    }
  }
  trait AgentConnectFixture {
    val agentNumber = "2500"
    val queueName = "sales"
    val agentChannel = new Channel("34800", "SIP/uwert", CallerId("eddie", "2014"),"34800", state = ChannelState.BUSY,monitored = MonitorState.ACTIVE )

    val agentConnectEvent = new AgentConnectEvent("")
    agentConnectEvent.setUniqueId(agentChannel.id)
    agentConnectEvent.setMemberName(s"Agent/$agentNumber")
    agentConnectEvent.setQueue(queueName)

  }
  "Ami event processor on Agent connect event" should {
    "publish a channel event with agent number" in new Helper() with AgentConnectFixture{

      val expectedChan = agentChannel.copy(agentNumber = Some(agentNumber))
      val chans = HashMap(agentChannel.id -> agentChannel )

      val updatedChans= eventProcessor.process(AmiEvent(agentConnectEvent), ChannelRepository(chans))

      verify(amiBus).publish(ChannelEvent(expectedChan))

      updatedChans(agentChannel.id).agentNumber should be(Some(agentNumber))
    }

  }

  "Ami event processor on new callerid event" should {
    "update caller id" in new Helper() {
      val uniqueId = "1432212391.17"
      val newCallerId = new NewCallerIdEvent("")
      newCallerId.setCallerIdName("John")
      newCallerId.setCallerIdNum("1200")
      newCallerId.setUniqueId(uniqueId)
      val channelIn = Channel(uniqueId, "SIP/barometrix_jyldev-00000007", CallerId("", ""), uniqueId)
      val chans = HashMap(uniqueId -> channelIn)

      val updatedChans = eventProcessor.process(AmiEvent(newCallerId), ChannelRepository(chans))

      updatedChans(uniqueId).callerId should be(CallerId("John", "1200"))
    }

  }
  "Ami event processor on Hangup event" should {
    "publish a channel event and delete the channel" in new Helper() {
      val channel = new Channel("4445", "SIP/uwert", CallerId("eddie", "2014"),"4445", state = ChannelState.OFFHOOK)
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)

      val updatedChans = eventProcessor.process(AmiEvent(hangupEvent),  ChannelRepository(HashMap(channel.id -> channel)))

      verify(amiBus).publish(ChannelEvent(channel.copy(state=ChannelState.HUNGUP)))

      updatedChans.get(channel.id) should be(None)

    }
    "not publish zombi channels" in new Helper {
      val channel = new Channel("1433244077.120", "Local/id-22@agentcallback-0000001c;1<ZOMBIE>", CallerId("eddie", "2014"),"1433244077.120", state = ChannelState.OFFHOOK)
      val hangupEvent = new HangupEvent("")
      hangupEvent.setUniqueId(channel.id)

      val updatedChans = eventProcessor.process(AmiEvent(hangupEvent),  ChannelRepository(HashMap(channel.id -> channel)))

      verifyZeroInteractions(amiBus)

      updatedChans.get(channel.id) should be(None)

    }
  }
  "Ami event processor on dial event" should {
    "publish and update channel on dialevent" in new Helper {
      val initialChannel = Channel("13216545.12", "SIP/wred", CallerId("tony", "2016"), agentNumber=Some("2500"), linkedChannelId = "13216545.12")
      val destChannel = Channel("14665454.54", "SIP/1k4yj2-00000038", CallerId("", ""), linkedChannelId = "14665454.54")

      val chans = HashMap(initialChannel.id -> initialChannel, destChannel.id -> destChannel)
      val dialEvt = new DialEvent("")
      dialEvt.setUniqueId(initialChannel.id)
      dialEvt.setDestUniqueId(destChannel.id)

      val updatedChans = eventProcessor.process(AmiEvent(dialEvt), ChannelRepository(chans))

      updatedChans(destChannel.id).linkedChannelId should be(initialChannel.linkedChannelId)
      updatedChans(destChannel.id).variables("XUC_OUTBOUND") should be("OtherEnd")
    }
  }
  "Ami event processor on masquerade event" should {
    class MasqueradeHelper extends Helper {
      def masqueradeEvent(clone:Channel, original:Channel) = {
        val masquerade = new MasqueradeEvent("")
        masquerade.setOriginal(original.name)
        masquerade.setClone(clone.name)
        masquerade
      }
      def chanRepo(clone:Channel, original:Channel):ChannelRepository = ChannelRepository(HashMap(original.id -> original, clone.id -> clone))

      def masq(clone:Channel, original:Channel):(MasqueradeEvent, ChannelRepository) = (masqueradeEvent(clone,original), chanRepo(clone,original))
    }
    "publish new channel data from clone channel with masquerade in variables (not saved)" in new MasqueradeHelper {
      val originalChannel = Channel("1434552423.403", "SIP/ihvbur-000000e7", CallerId("Brucé Wail","1000"),state=ChannelState.RINGING, linkedChannelId = "1434552421.400",variables = Map("Var"->"Value"))
      val cloneChannel = Channel("1434552435.405", "SIP/vs4unqzp-000000e9", CallerId("Fab GOODBYE","1182"),state=ChannelState.UP, linkedChannelId = "1434552435.404")

      val (masquerade, chanRepo) = masq(cloneChannel, originalChannel)

      val updatedChans = eventProcessor.process(AmiEvent(masquerade), chanRepo)

      updatedChans(originalChannel.id).callerId should be(cloneChannel.callerId)
      updatedChans(originalChannel.id).state should be(cloneChannel.state)
      updatedChans(originalChannel.id).variables.get("Masquerade") should be(None)
      verify(amiBus).publish(ChannelEvent(updatedChans(originalChannel.id).addVariables(Map("Masquerade"->"1"))))
    }
    "if callerid changed publish original hangup (transfert compmleted)" in new MasqueradeHelper {
      val originalChannel = Channel("1434552423.403", "SIP/ihvbur-000000e7", CallerId("Brucé Wail","1000"),state=ChannelState.RINGING, linkedChannelId = "1434552421.400",variables = Map("Var"->"Value"))
      val cloneChannel = Channel("1434552435.405", "SIP/vs4unqzp-000000e9", CallerId("Fab GOODBYE","1182"),state=ChannelState.UP, linkedChannelId = "1434552435.404")

      val (masquerade, chanRepo) = masq(cloneChannel, originalChannel)

      val updatedChans = eventProcessor.process(AmiEvent(masquerade), chanRepo)

      verify(amiBus).publish(ChannelEvent(originalChannel.withChannelState(ChannelState.HUNGUP)))

    }
    "if callerid does not change (agent masquerade) do not hungup original channel" in new MasqueradeHelper {
      val originalChannel = Channel("1434693269.1", "Local/id-19@agentcallback-00000000;1",  CallerId("Irène Dupont","1118"),state=ChannelState.RINGING, linkedChannelId = "1434693267.0",variables = Map("Var"->"Value"))
      val cloneChannel = Channel("1434693269.3", "SIP/g2lhmi-00000001", CallerId("Irène Dupont","1118"),state=ChannelState.UP, linkedChannelId = "1434693267.0")

      val (masquerade, chanRepo) = masq(cloneChannel, originalChannel)

      val updatedChans = eventProcessor.process(AmiEvent(masquerade), chanRepo)

      verify(amiBus, never).publish(ChannelEvent(originalChannel.withChannelState(ChannelState.HUNGUP)))

    }
  }
}
