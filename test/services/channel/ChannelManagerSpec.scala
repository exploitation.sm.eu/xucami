package services.channel

import java.beans.PropertyChangeEvent
import java.util

import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.asteriskjava.live.AsteriskChannel
import org.asteriskjava.live.{ChannelState => AstChannelState}
import org.asteriskjava.manager.action.{GetVarAction, UnpauseMonitorAction, PauseMonitorAction}
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.{GetVarResponse, ManagerResponse}
import org.mockito.Mockito.{when, verify, verifyZeroInteractions, times, reset}
import org.mockito.Matchers.any
import org.scalatest.mock.MockitoSugar
import org.specs2.mock.mockito.ArgumentCapture
import services.XucAmiBus
import services.XucAmiBus._
import services.channel.ChannelManager.Channels
import xivo.xucami.models._
import scala.collection.immutable.HashMap

class ChannelManagerSpec extends TestKitSpec("ChannelManagerSpec")
  with MockitoSugar {

  class Helper() {
    val agentCalled = new AgentCalledEvent("")
    agentCalled.setAgentCalled("Local/id-6@agentcallback")
    agentCalled.setAgentName("Agent/2200")
    agentCalled.setUniqueId("1416910889.110")
    agentCalled.setQueue("irs")
    agentCalled.setCallerIdName("hawkeye")
    agentCalled.setCallerIdNum("1002")

    val amiBus = mock[XucAmiBus]
    val repoUpdater = mock[(Channel,Channels)=>Channels]

    val channels = mock[Channels]

    trait TestEventProcessor extends EventProcessor {
      override val processor = mock[AmiEventProcessor]
    }
    def actor(channels: Channels = channels) = {
      val sa = TestActorRef(new ChannelManager(amiBus, addNewChannel = repoUpdater, channels= channels) with TestEventProcessor)
      (sa, sa.underlyingActor)
    }
  }

  override def afterAll() {
    system.shutdown()
  }

  "ChannelManager" should {

    "subscribe to the ami bus for AmiEvents, AmiResponses and ChannelActionRequests" in new Helper() {
      val (ref, a) = actor()
      verify(amiBus).subscribe(ref, AmiType.AmiEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiResponse)
      verify(amiBus).subscribe(ref, AmiType.ChannelActionRequest)
    }

    "create a channel and publish on newChannel event" in new Helper {
      val (ref, a) = actor()
      val uniqueId = "1432139825.43"
      val chanName = "Local/3099@default-0000000a;1"
      val callerId = CallerId("jack", "1200")
      val newChanEvent = new NewChannelEvent("")
      newChanEvent.setUniqueId(uniqueId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      newChanEvent.setExten("3200")

      val newChannel = Channel(uniqueId,chanName, callerId, linkedChannelId = uniqueId, exten=Some("3200"))

      ref ! AmiEvent(newChanEvent)

      verify(amiBus).publish(ChannelEvent(newChannel))
      verify(repoUpdater)(newChannel, channels)

    }
    "process ami events" in new Helper {
      val (ref, a) = actor()
      val newStateEvent = AmiEvent(new NewStateEvent(""))

      ref ! newStateEvent

      verify(a.processor).process(newStateEvent, ChannelRepository(channels))
    }

    "publish a channel event on a PropertyChangeEvent with monitored=true" in new Helper() {
      val (ref, a) = actor()
      val initialChannel = Channel("646654546.56", "SIP/wert", CallerId("eddie", "2014"),"646654546.56", ChannelState.BUSY)
      val channel = mock[AsteriskChannel]
      when(channel.getName()).thenReturn("SIP/wert")
      when(channel.getId()).thenReturn("35700")
      val event = new PropertyChangeEvent(channel, "monitored", false, true)
      a.process(HashMap("35700" -> initialChannel))(event)

      val expectedChannel = new Channel("646654546.56", "SIP/wert", CallerId("eddie", "2014"),"646654546.56")
      expectedChannel.state = ChannelState.BUSY
      expectedChannel.monitored = MonitorState.ACTIVE
      verify(amiBus).publish(ChannelEvent(expectedChannel))
    }



    "request PauseMonitor on the corresponding ChannelRequest" in new Helper() {
      val (ref, a) = actor()
      val request = ChannelRequest(ChannelActionRequest(Some("444332"), ChannelAction.PauseMonitor))
      val channel = new Channel("444332.22", "SIP/uwert", CallerId("eddie", "2014"),"444332.10", agentNumber = Some("444332"))
      channel.monitored = MonitorState.ACTIVE
      a.process(HashMap("444332" -> channel))(request)

      val arg = new ArgumentCapture[AmiAction]()
      verify(amiBus).publish(arg.capture)
      arg.value.message match {
        case pause: PauseMonitorAction =>
          pause.getChannel() shouldBe "SIP/uwert"
        case any =>
          fail(s"Should get PauseMonitorAction, got: $any")
      }
      arg.value.reference match {
        case Some(s)=>
          s.shouldEqual("444332.22")
        case any =>
          fail(s"Should get channelId, got: $any")
      }
    }

    "request UnpauseMonitor on the corresponding ChannelRequest" in new Helper() {
      val (ref, a) = actor()
      val request = ChannelRequest(ChannelActionRequest(Some("365"), ChannelAction.UnpauseMonitor))
      val channel = new Channel("567", "SIP/unpause", CallerId("eddie", "2014"),"567", agentNumber = Some("365"))
      channel.monitored = MonitorState.ACTIVE
      a.process(HashMap("567" -> channel))(request)

      val arg = new ArgumentCapture[AmiAction]()
      verify(amiBus).publish(arg.capture)
      arg.value.message match {
        case pause: UnpauseMonitorAction =>
          pause.getChannel() shouldBe "SIP/unpause"
        case any =>
          fail(s"Should get UnpauseMonitorAction, got: $any")
      }
      arg.value.reference match {
        case Some(s)=>
          s.shouldEqual("567")
        case any =>
          fail(s"Should get channelId, got: $any")
      }
    }

    "publish a channel event on successful pauseMonitor" in new Helper() {
      val (ref, a) = actor()
      val response = new ManagerResponse()
      response.setResponse("Success")
      val request = new PauseMonitorAction()
      val richResponse = AmiResponse(response, Some(AmiAction(request, Some("7789"))))
      val channel = new Channel("7789", "SIP/uwert", CallerId("eddie", "2014"),"7789")
      channel.monitored = MonitorState.ACTIVE
      a.process(HashMap("7789" -> channel))(richResponse)

      channel.monitored = MonitorState.PAUSED
      verify(amiBus).publish(ChannelEvent(channel))
    }

    "publish a channel event on successful unpauseMonitor" in new Helper() {
      val (ref, a) = actor()
      val response = new ManagerResponse()
      response.setResponse("Success")
      val request = new UnpauseMonitorAction()
      val richResponse = AmiResponse(response, Some(AmiAction(request, Some("8899"))))
      val channel = new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"),"8899")
      channel.monitored = MonitorState.PAUSED
      a.process(HashMap("8899" -> channel))(richResponse)

      channel.monitored = MonitorState.ACTIVE
      verify(amiBus).publish(ChannelEvent(channel))
    }

    "publish a channel event with monitor state paused on variable MONITOR_PAUSED" in new Helper {
      val (ref, a) = actor()
      val response = new GetVarResponse()
      response.setResponse("Success")
      val attr = new util.HashMap[String,Object]()
      attr.put("variable","MONITOR_PAUSED")
      attr.put("value","true")
      response.setAttributes(attr)
      val request = new GetVarAction()

      val richResponse = AmiResponse(response, Some(AmiAction(request, Some("8899"))))
      val channel = new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"), "8899")

      a.process(HashMap("8899" -> channel))(richResponse)

      channel.monitored = MonitorState.PAUSED
      verify(amiBus).publish(ChannelEvent(channel))

    }

    "ignore failed Responses" in new Helper() {
      val (ref, a) = actor()
      val response = new ManagerResponse()
      response.setResponse("Error")
      val request = new PauseMonitorAction()
      request.setChannel("7789")
      val richResponse = AmiResponse(response, Some(AmiAction(request)))
      val channel = new Channel("7789", "SIP/uwert", CallerId("eddie", "2014"), "7789")
      channel.monitored = MonitorState.ACTIVE
      import ChannelManager.Channels
      a.process(channels)(richResponse)

      verifyZeroInteractions(channels)
      verify(amiBus, times(0)).publish(any())
    }

  }

  "Channel repo" should {
    "update channel in repo" in {
      val chanId = "13213321.23"
      val channel = Channel(chanId,"SIP/ihvbur-00000017", CallerId("JYves","44200"), chanId, variables = Map("One"->"1"))
      val channels:Channels = HashMap(chanId -> channel)

      val updatedChannel = Channel(chanId,"SIP/ihvbur-00000017", CallerId("JYves","44200"), chanId)

      val updatedRepo = ChannelRepo.addNewChannel(channel, channels)

      updatedRepo(chanId) should be(channel)
    }
  }
}