package xivo.xucami.models

import java.util.HashMap

import org.asteriskjava.live.AsteriskChannel
import org.asteriskjava.live.{CallerId => AstCallerId}
import org.asteriskjava.live.{ChannelState => AstChannelState}
import org.asteriskjava.manager.event.{NewStateEvent, NewCallerIdEvent, NewChannelEvent}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{Matchers, WordSpec}
import org.mockito.Mockito.when


class ChannelSpec extends WordSpec with Matchers with MockitoSugar{

  "Channel" should {
    "create new Channel from Asterisk channel" in {
      var variables = new HashMap[String,String]()
      variables.put("KEY","VALUE")

      val astChannel = mock[AsteriskChannel]
      val astCallerId = new AstCallerId("nix", "2255")
      val linkedChannel = mock[AsteriskChannel]
      when(astChannel.getId()).thenReturn("12345")
      when(astChannel.getName()).thenReturn("testName")
      when(astChannel.getCallerId()).thenReturn(astCallerId)
      when(astChannel.getState()).thenReturn(AstChannelState.BUSY)
      when(linkedChannel.getId).thenReturn("5555")
      when(astChannel.getLinkedChannel).thenReturn(linkedChannel)
      when(astChannel.getVariables).thenReturn(variables)

      val channel = Channel(astChannel)
      channel.id shouldBe "12345"
      channel.name shouldBe "testName"
      channel.callerId.name shouldBe "nix"
      channel.callerId.number shouldBe "2255"
      channel.state shouldBe ChannelState.BUSY
      channel.linkedChannelId shouldBe "5555"
      channel.monitored shouldBe MonitorState.DISABLED
      channel.variables shouldBe Map[String,String]("KEY"->"VALUE")
    }


    "be created from new channel event" in {
      val uniqueId = "1432139825.43"
      val chanName = "Local/3099@default-0000000a;1"
      val callerId = CallerId("jack", "1200")
      val newChanEvent = new NewChannelEvent("")
      newChanEvent.setUniqueId(uniqueId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      newChanEvent.setChannelState(0)

      val expected = Channel(uniqueId, chanName, callerId, state=ChannelState.DOWN, linkedChannelId = uniqueId)
      val channel = Channel(newChanEvent)

      channel should be(expected)
    }
    "be created with default state UNITIALIZED" in {
      val uniqueId = "1432139825.43"
      val chanName = "Local/3099@default-0000000a;1"
      val callerId = CallerId("jack", "1200")
      val newChanEvent = new NewChannelEvent("")
      newChanEvent.setUniqueId(uniqueId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      val expected = Channel(uniqueId, chanName, callerId, state=ChannelState.UNITIALIZED, linkedChannelId = uniqueId)
      val channel = Channel(newChanEvent)

      channel should be(expected)

    }
    "Do not replace callerid if callerId name and number are already set" in {

      Channel("1433776805.61","SIP/01025555-00000023",CallerId("Name","1200"),"1433776805.61").withCallerId(CallerId("newName","2000")).callerId should be(CallerId("Name","1200"))

      Channel("1433776805.61","SIP/01025555-00000023",CallerId("","1200"),"1433776805.61").withCallerId(CallerId("newName","2000")).callerId should be(CallerId("newName","2000"))
      Channel("1433776805.61","SIP/01025555-00000023",CallerId(null,"1200"),"1433776805.61").withCallerId(CallerId("newName","2000")).callerId should be(CallerId("newName","2000"))
      Channel("1433776805.61","SIP/01025555-00000023",CallerId("Name",""),"1433776805.61").withCallerId(CallerId("newName","2000")).callerId should be(CallerId("newName","2000"))
      Channel("1433776805.61","SIP/01025555-00000023",CallerId("Name",null),"1433776805.61").withCallerId(CallerId("newName","2000")).callerId should be(CallerId("newName","2000"))

    }
    "Do not replace callerid on new state" in {
      var chan = Channel("1433776805.70","SIP/01025555-0000003f",CallerId("Name","1200"),"1433776805.70")

      val newStateEvent = new NewStateEvent("")
      newStateEvent.setChannelState(4)
      newStateEvent.setCallerIdName("newName")
      newStateEvent.setCallerIdNum("1234")

      chan.withState(newStateEvent).callerId should be(CallerId("Name","1200"))
    }
    "Replace caller id on new state if agent callback channel" in {
      var chan = Channel("1434368938.106","Local/id-22@agentcallback-00000014;1f",CallerId("Name","1200"),"1434368938.106")
      val newStateEvent = new NewStateEvent("")
      newStateEvent.setChannelState(4)
      newStateEvent.setCallerIdName("newName")
      newStateEvent.setCallerIdNum("1234")

      chan.withState(newStateEvent).callerId should be(CallerId("newName","1234"))

    }
  }

}
